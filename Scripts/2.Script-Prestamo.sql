-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tp1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tp1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tp1` DEFAULT CHARACTER SET utf8 ;
USE `tp1` ;

-- -----------------------------------------------------
-- Table `tp1`.`prestamo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tp1`.`prestamo` (
  `idPrestamo` INT(11) NOT NULL AUTO_INCREMENT,
  `idCliente` INT(11) NOT NULL,
  `fecha` DATE NOT NULL,
  `monto` DOUBLE NOT NULL,
  `interes` DOUBLE NOT NULL,
  `cantCuotas` INT(11) NOT NULL,
  PRIMARY KEY (`idPrestamo`),
  INDEX `fk_cliente` (`idCliente` ASC),
  CONSTRAINT `fk_cliente`
    FOREIGN KEY (`idCliente`)
    REFERENCES `tp1`.`cliente` (`idCliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
