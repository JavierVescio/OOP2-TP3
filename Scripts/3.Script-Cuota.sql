-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema tp1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tp1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tp1` DEFAULT CHARACTER SET utf8 ;
USE `tp1` ;

-- -----------------------------------------------------
-- Table `tp1`.`cuota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tp1`.`cuota` (
  `idCuota` INT(11) NOT NULL AUTO_INCREMENT,
  `idPrestamo` INT(11) NOT NULL,
  `nroCuota` DOUBLE NOT NULL,
  `fechaVencimiento` DATE NOT NULL,
  `saldoPendiente` DOUBLE NOT NULL,
  `amortizacion` DOUBLE NOT NULL,
  `interesCuota` DOUBLE NOT NULL,
  `cuota` DOUBLE NOT NULL,
  `deuda` DOUBLE NOT NULL,
  `cancelada` BIT(1) NOT NULL,
  `fechaDePago` DATE NULL DEFAULT NULL,
  `punitorios` DOUBLE NOT NULL,
  PRIMARY KEY (`idCuota`),
  INDEX `fk_prestamo_idx` (`idPrestamo` ASC),
  CONSTRAINT `fk_prestamo`
    FOREIGN KEY (`idPrestamo`)
    REFERENCES `tp1`.`prestamo` (`idPrestamo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
