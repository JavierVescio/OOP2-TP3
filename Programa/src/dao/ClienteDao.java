package dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import datos.Cliente;

public class ClienteDao {
	private static Session session;
	private Transaction tx;

	private void iniciaOperacion() throws HibernateException {
		session = HibernateUtil.getSessionFactory().openSession();
		tx = session.beginTransaction();
	}

	private void manejaExcepcion(HibernateException he) throws HibernateException {
		tx.rollback();
		throw new HibernateException("ERROR en la capa de acceso a datos", he);
	}

	public int agregar(Cliente objeto) throws HibernateException  {
		int id=0;
		try {
			iniciaOperacion();
			id = Integer.parseInt(session.save(objeto).toString());
			tx.commit();
		}catch(HibernateException he) {
			manejaExcepcion(he);
			throw he;
		}finally {
			session.close();
		}
		return id;
	}

	public void actualizar(Cliente objeto) throws HibernateException {
		try {
			iniciaOperacion();
			session.update(objeto);
			tx.commit();
		}catch(HibernateException he) {
			manejaExcepcion(he);
			throw he;
		}finally {
			session.close();
		}
	}

	public void eliminar(Cliente objeto) throws HibernateException {
		try {
			iniciaOperacion();
			session.delete(objeto);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}
	}
	
	public Cliente traerCliente( int dni) throws HibernateException {
		Cliente objeto = null ;
		try {
			iniciaOperacion();
			String hql = "from Cliente c where c.dni="+dni;
			objeto = (Cliente)session.createQuery(hql).uniqueResult();
		} finally {
			session.close();
		}
		return objeto;
	}
	
	public Cliente traerCliente( long idCliente) throws HibernateException {
		Cliente objeto = null ;
		try {
			iniciaOperacion();
			objeto = (Cliente)session.get(Cliente.class, idCliente);
		} finally {
			session.close();
		}
		return objeto;
	}
	
	/*traerClienteYPrestamos V1
	 * Esta funcion cumple con lo mismo que V2. El unico cambio es en la linea de objeto = (Cliente)...
	 */ 
	public Cliente traerClienteYPrestamos(long idCliente) throws HibernateException {
		Cliente objeto = null;
		try {
			iniciaOperacion();
			objeto = (Cliente)session.get(Cliente.class, idCliente);
				/*Hasta aca es todo identico a traerCliente(long idCliente).
				 * La linea que marca la diferencia es la que sigue.
				 * De alguna manera, logra cargar el atributo prestamos de la clase cliente
				 * el cual es un set (como una lista) que contiene todos los prestamos del cliente.
				 * */
			Hibernate.initialize(objeto.getPrestamos());
		}finally {
			session.close();
		}
		
		return objeto;
	}
	
	//traerClienteYPrestamos V2. Esta es la funcion original que aparecia en el TP
	/*public Cliente traerClienteYPrestamos(long idCliente) throws HibernateException {
		Cliente objeto = null;
		try {
			iniciaOperacion();
			String hql="from Cliente where idCliente ="+idCliente;
				//from "cliente" con "c" minuscula estaria mal, a pesar de que en el Workbench este asi.
				//Aca tenes que ver como registraste el mapeo. <class name="datos.Cliente" table="cliente">
				//Cliente esta en mayuscula.
			objeto=(Cliente)session.createQuery(hql).uniqueResult();
			Hibernate.initialize(objeto.getPrestamos());
		}finally {
			session.close();
		}
		
		return objeto;
	}*/

	@SuppressWarnings("unchecked")
	public List<Cliente> traerCliente() throws HibernateException {
		List<Cliente> lista=null;

		try {
			iniciaOperacion();
			lista= session .createQuery( "from Cliente c order by c.apellido asc c.nombre asc").list();
		}finally {
			session.close();
		}
		return lista;
	}
}
