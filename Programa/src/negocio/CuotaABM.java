package negocio;

import dao.CuotaDao;
import datos.Cuota;

public class CuotaABM {
	private CuotaDao dao = new CuotaDao();
	
	public void modificar(Cuota cuota) throws Exception { //Esta bien?
		dao.actualizar(cuota);
	}
	
	public Cuota traerCuota(long idCuota) throws Exception {
		Cuota c = dao.traerCuota(idCuota);
		if (c == null){
			throw new Exception("La cuota no existe");
		}
		else{
			return c;
		}
	}
}
