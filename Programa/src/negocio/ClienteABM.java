package negocio;

import java.util.GregorianCalendar;
import java.util.List;
import dao.ClienteDao;
import datos.Cliente;

public class ClienteABM {
	ClienteDao dao = new ClienteDao();

	public Cliente traerCliente(long idCliente) throws Exception{
		Cliente c= dao.traerCliente(idCliente);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}

	public Cliente traerCliente(int dni) throws Exception {
		Cliente c = dao.traerCliente(dni);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}
	
	public Cliente traerClienteYPrestamos(long idCliente) throws Exception{
		Cliente c= dao.traerClienteYPrestamos(idCliente);
		if (c==null){
			throw new Exception("No existe el cliente");
		}
		return c;
	}

	public int agregar(String apellido, String nombre, int dni, GregorianCalendar fechaDeNacimiento) throws Exception {
		Cliente c = dao.traerCliente(dni);
		if (c == null)
			c = new Cliente(apellido, nombre, dni,fechaDeNacimiento);
		else
			throw new Exception("El cliente con el dni ingresado ya existe!");
		return dao.agregar(c);
	}

	public void modificar(Cliente c) throws Exception{
		dao.actualizar(c);
	}

	public void eliminar(long idCliente) throws Exception { /*en este caso es f�sica en gral. no se se
		aplicar�a este caso de uso, si se hiciera habr�a que validar que el cliente no tenga
		dependencias*/
		Cliente c = dao.traerCliente(idCliente);
		if (c==null){
			throw new Exception("No hay cliente para eliminar");
		}
		else
			dao.eliminar(c);
	}
	
	public List<Cliente> traerCliente(){ 
		return dao.traerCliente();
	}
}
