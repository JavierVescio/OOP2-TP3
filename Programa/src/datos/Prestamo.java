package datos;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import modelo.Funciones;

public class Prestamo {
	private long idPrestamo;
	private GregorianCalendar fecha;
	private double monto;
	private double interes;
	private int cantCuotas;
	private Cliente cliente;
	private Set<Cuota> cuotas;
	
	public Prestamo() {}
	
	public Prestamo(GregorianCalendar fecha, double monto, double interes, int cantCuotas, Cliente cliente) {
		super();
		this.fecha = fecha;
		this.monto = monto;
		this.interes = interes;
		this.cantCuotas = cantCuotas;
		this.cliente = cliente;
		setCuotas();
	}
	
	public long getIdPrestamo() {
		return idPrestamo;
	}
	
	protected void setIdPrestamo(long idPrestamo) {
		this.idPrestamo = idPrestamo;
	}
	
	public GregorianCalendar getFecha() {
		return fecha;
	}

	public void setFecha(GregorianCalendar fecha) {
		this.fecha = fecha;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}

	public int getCantCuotas() {
		return cantCuotas;
	}

	public void setCantCuotas(int cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Set<Cuota> getCuotas() {
		return cuotas;
	}
	
	public void setCuotas(Set<Cuota> cuotas) {
		this.cuotas = cuotas;
	}
	
	/*
	 --->setCuotas() *** Imprimir Prestamo y cuotas a pagar **** 
	 Préstamo: $6000.0 
	 Fecha: 10/01/2016 
	 Interés: 12.0 
	 Cant.de Cuotas: 8 
	 Cliente:  4 Vranic Alejandra 44444444 
	 Nro.1 F.Vto: 10/02/2016 
	 
Saldo pendiente: $6000.0 Amortización: $487.81 Interés: $720.0 Cuota: $1207.81 Deuda: $5512.18 Nro.2 F.Vto: 10/03/2016 
Saldo pendiente: $5512.18 Amortización: $546.35 Interés: $661.46 Cuota: $1207.81 Deuda: $4965.82 Nro.3 F.Vto: 11/04/2016 
Saldo pendiente: $4965.82 Amortización: $611.91 Interés: $595.89 Cuota: $1207.81 Deuda: $4353.91 Nro.4 F.Vto: 10/05/2016 
Saldo pendiente: $4353.91 Amortización: $685.34 Interés: $522.46 Cuota: $1207.81 Deuda: $3668.56 Nro.5 F.Vto: 10/06/2016 
Saldo pendiente: $3668.56 Amortización: $767.59 Interés: $440.22 Cuota: $1207.81 Deuda: $2900.97 Nro.6 F.Vto: 11/07/2016 
Saldo pendiente: $2900.97 Amortización: $859.7 Interés: $348.11 Cuota: $1207.81 Deuda: $2041.27 Nro.7 F.Vto: 10/08/2016 
Saldo pendiente: $2041.27 Amortización: $962.86 Interés: $244.95 Cuota: $1207.81 Deuda: $1078.4 Nro.8 F.Vto: 12/09/2016 
Saldo pendiente: $1078.4 Amortización: $1078.4 Interés: $129.4 Cuota: $1207.81 Deuda: $0.0
	 * */
	public void setCuotas() {
		cuotas = new HashSet<Cuota>();
		int nroCuota;
		double saldoPendiente = this.monto;
		double amortizacion;
		double interesCuota;
		double cuota;
		double deuda;
		
		int dia, mes, anio;
		dia = fecha.get(Calendar.DAY_OF_MONTH);
		mes = fecha.get(Calendar.MONTH);
		anio = fecha.get(Calendar.YEAR);
		
		GregorianCalendar fechaVencimiento = new GregorianCalendar(anio,mes,dia);
		
		for(nroCuota=1;nroCuota<=cantCuotas;nroCuota++){
			Cuota objCuota = new Cuota();
	
			amortizacion = (saldoPendiente*(interes*0.01))/((Math.pow(1 + (interes*0.01),cantCuotas+1-nroCuota))-1);
		    interesCuota = saldoPendiente*interes;
		    cuota = amortizacion+(interesCuota*0.01);
		    deuda = saldoPendiente-amortizacion;
		    
		    fechaVencimiento.add(Calendar.MONTH,1);
		    Funciones.siguienteFechaHabil(fechaVencimiento); //revisar
			dia = fechaVencimiento.get(Calendar.DAY_OF_MONTH);
			mes = fechaVencimiento.get(Calendar.MONTH);
			anio = fechaVencimiento.get(Calendar.YEAR);
		    
		    
			objCuota.setPrestamo(this);
			objCuota.setNroCuota(nroCuota);
			objCuota.setFechaVencimiento(new GregorianCalendar(anio,mes,dia));
		    objCuota.setSaldoPendiente(saldoPendiente);
			objCuota.setAmortizacion(amortizacion);
			objCuota.setInteresCuota(interesCuota);
			objCuota.setCuota(cuota);
			objCuota.setDeuda(deuda);
			objCuota.setCancelada(false);
			objCuota.setPunitorios(0);
			
			cuotas.add(objCuota);

			saldoPendiente = saldoPendiente-amortizacion;
		}
	}
	


	public String toString() {
		String prestamo = "Prestamo: $ " +monto+"\nFecha: "+Funciones.traerFechaCorta(fecha)+"\nInteres: "+interes+"\nCant. de cuotas: "+cantCuotas;
		return prestamo;
	}
}
