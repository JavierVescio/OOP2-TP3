package test;

import java.util.List;

import datos.Cliente;
import negocio.ClienteABM;

public class TestTraerTodosLosClientes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClienteABM abm= new ClienteABM();
		List<Cliente> lista_clientes = abm.traerCliente();
		
		for(Cliente cliente: lista_clientes) {
			System.out.println("..");
			System.out.println(cliente);
			System.out.println("..");
		}
	}

}
